# Pente Game Server

Multiple clients can connect to the server locally or over the network as long as they have the server's IP address.

The Grizzly HTTP server is created in src/main/java/server/ServerMain.java, on localhost port 5004.